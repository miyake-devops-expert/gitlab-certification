# Comandos Git

## git Clone
Clona o repositório
``` bash
git clone 'nome do repo'
```

## git add 
Adiciona um arquivo a staging ou index area
``` bash
git add 'nome do arquivo'
#ou
git add . #para todos arquivos
```

## git status
Mostra o status do repositório
```bash
git status
```

## git commit 
Valida (comita) a mudança ocorrida, serve como pontos de referencia e de teretorno em caso de erro na timeline de vida do arquivo/codigo.
```bash
git commit -m 'mensagem'
```

## git push
Envia os arquivos após o commit para o repositorio.
```bash
git push
```

## git pull
Recupera os arquivos atualizados do repositório na nuvem para o repo local.
```bash
git pull
```

## git branch
Permite criar ramificações para o repositorio, o branch padrão é a master. Isso permite fazer alterações e deppois mesclar elas com a master.
```bash
#permite listar as branchs existentes
git branch

#permite mudar de branch
git checkout 'nome da branch'

#permite criar uma branch nova e mudar para ela
git checkout -b 'nome da branch'
``` 


